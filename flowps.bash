#!/usr/bin/env bash
python flowps.py \
    --train-file data/TARGET20_with_busulfan.csv \
    --min-surround 0 --max-surround 10 \
    --min-neighbours 32 --max-neighbours 45
